#!/usr/bin/env python

import re, datetime

starTime = datetime.datetime.now()

term1 = re.compile('EDGE-ACL-IPv4-IN')
term2 = re.compile('10.10.10')

fileList = [ 'messages-*.log' ]

for i in fileList:
  with open (i, 'r') as f:
    for i in f.readlines():
      if re.search(term1, i) and re.search(term2, i):
        print(i)
