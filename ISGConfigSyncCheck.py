#!/usr/bin/env python

# Ver 1.0 - Intial Commit
# Ver 1.1 - + socket
# Ver 1.2 - + try/except in if
# Ver 1.3 - + try/except in elif

import paramiko, time, smtplib, socket

username = 'username'
password = 'password'
device = ['device1.sw11.lab','device2.sw11.lab', 'device3.sw11.lab', 'device4.sw11.lab']
max_buffer = 65535
FROM = 'lvrfrc87@gmail.com
TOALL = ['justfew@email.com', 'all@email.com']
TOIS = ['justfew@email.com']
smtpObj = smtplib.SMTP('10.0.0.1')

def main():
    for i in device:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(socket.gethostbyname(i), username=username, password=password, look_for_keys=False, allow_agent=False)
        connection = ssh.invoke_shell()
        connection.send('\n')
        connection.send('exec nsrp sync global-config check-sum\n')
        time.sleep(1)
        BODY = connection.recv(max_buffer)
        if 'Warning: configuration out of sync' in BODY:
            try:
                SUBJECT = '{} external firewall out-of-sync - RAISE P3 to All'.format(i)
                smtpObj.sendmail(FROM, TOALL, 'Subject: {}\n\n {}:'.format(SUBJECT,BODY))
                print 'Warning: configuration out of sync - Successfully sent email'
            except Exception:
                print 'SMTP server not reachable - e-mail not sent'
        elif 'configuration in sync' in BODY:
            try:
                SUBJECT = '{} external firewall config in sync - Everything is OK'.format(i)
                smtpObj.sendmail(FROM, TOIS, 'Subject: {}\n\n {}:'.format(SUBJECT,BODY))
                print 'Config in sync - Everything is OK'
            except Exception:
                print 'SMTP server not reachable - e-mail not sent'
        else:
            print 'Something wrong with the script...noob!'

if __name__ == '__main__':
    main()

#f20180404
