#!/usr/bin/env python
import time, re, getpass
from netmiko import ConnectHandler

username = raw_input('Username: ')
password = getpass.getpass('Password: ')
PRMFW01 = {'device_type':'juniper', 'ip':'10.40.0.4', 'username':username, 'password':password, 'verbose':'False'}

def nat250():
    net_connect = ConnectHandler(**PRMFW01)
    d_nat_250 = net_connect.send_command('show configuration security nat destination | match .250. | display set')
    time.sleep(1)
    pool_250 = re.findall(r'(?<=pool\s)[-\w+]*', d_nat_250)
    for i in pool_250:
        a = net_connect.send_command('show configuration security nat destination rule-set FRONTEND_DNAT rule %s match destination-address | display set' % i)
        b = re.findall(r'(?<=destination-address\s)[\w.]*', a)
        print b
    print "This is .250 pool DNAT"

def nat251():
    net_connect = ConnectHandler(**PRMFW01)
    d_nat_251 = net_connect.send_command('show configuration security nat destination | match .251. | display set')
    time.sleep(1)
    pool_251 = re.findall(r'(?<=pool\s)[-\w+]*', d_nat_251)
    for i in pool_251:
        a = net_connect.send_command('show configuration security nat destination rule-set FRONTEND_DNAT rule %s match destination-address | display set' % i)
        b = re.findall(r'(?<=destination-address\s)[\w.]*', a)
        print b
    print "This is .251 pool DNAT"

def main():
    nat250()
    nat251()

main()
#f20180301
